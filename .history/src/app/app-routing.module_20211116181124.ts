import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IntroGuard } from '../../.history/src/app/gards/intro.guard_20211116181025';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canLoad : [IntroGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'intro',
    loadChildren: () => import('../app/pages/intro/intro.module').then(m => m.IntroPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('../app/pages/login/login.module').then(m => m.LoginPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'pwdforget',
    loadChildren: () => import('../app/pages/pwdforget/pwdforget.module').then(m => m.PwdforgetPageModule)
  },
  {
    path: 'newpwd',
    loadChildren: () => import('../app/pages/newpwd/newpwd.module').then(m => m.NewpwdPageModule)
  },
  {
    path: 'meetings',
    loadChildren: () => import('./pages/meetings/meetings.module').then(m => m.MeetingsPageModule)
  },
  {
    path: 'taking',
    loadChildren: () => import('./pages/appointment-taking/appointment-taking.module').then(m => m.AppointmentTakingPageModule)
  },
  {

    path: 'details',
    loadChildren: () => import('./pages/details/details.module').then(m => m.DetailsPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then(m => m.IntroPageModule)
  },
  {
    path: 'populardoctors',
    loadChildren: () => import('./pages/populardoctors/populardoctors.module').then(m => m.PopulardoctorsPageModule)
  },
  {
    path: 'validation',
    loadChildren: () => import('./pages/doublevalidation/doublevalidation.module').then(m => m.DoublevalidationPageModule)
  },
  {
    path: 'doctors',
    loadChildren: () => import('./pages/doctors/doctors.module').then( m => m.DoctorsPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
