import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  router: Router;

  constructor(
    private injector: Injector,
  ) {
    this.router = this.injector.get<Router>(Router);

  }

}
