import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
export const INTRO_KEY = "intro-has-seean";
import { NativeStorage } from '@ionic-native/native-storage/ngx';


@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {

  constructor(private router: Router, private Storage :) {

  }

  async canLoad(): Promise<boolean> {

    const hasSeenIntro = Storage.get({ key: INTRO_KEY });

    if (hasSeenIntro && (await Storage.get(INTRO_KEY).value === "true")) {
      return true;
    } else {
      this.router.navigate(['/intro']);
    }


    return true;
  }
}
