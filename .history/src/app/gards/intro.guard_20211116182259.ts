import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
export const INTRO_KEY = "intro-has-seean";
import { NativeStorage } from '@ionic-native/native-storage/ngx';


@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {

  constructor(private router: Router, private Storage : NativeStorage) {

  }

  async canLoad(): Promise<boolean> {

    const hasSeenIntro = this.Storage.get({ key: INTRO_KEY });

    // if (hasSeenIntro && (await this.Storage.getItem(INTRO_KEY)=== "true")) {
    //   return true;
    // } else {
    //   this.router.navigate(['/intro']);
    // }


    this.Storage.getItem(INTRO_KEY)
      .then(
        data => {if(data )},
        error => console.error(error)
      );
  }
}
