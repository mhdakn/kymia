import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
export const INTRO_KEY = "intro-has-seean";
import { NativeStorage } from '@ionic-native/native-storage/ngx';


@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {

  constructor(private router: Router, private Storage : NativeStorage) {

  }

  async canLoad(): Promise<boolean> {

    this.Storage.getItem(INTRO_KEY)
      .then(
        data => { return (data === true) },
        error => this.router.navigate(['/intro'])
      );
  }
}
