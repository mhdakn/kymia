import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
export const INTRO_KEY = "intro-has-seean";
import { Storage } from 'src/app/service/storage.service';


@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {

  constructor(private router: Router, private Storage: Storage) {
    this.Storage.create();
  }

  async canLoad(): Promise<any> {
    this.Storage.get(INTRO_KEY)
      .then(
        data => { return (data === true) },
        error => this.router.navigate(['/intro'])
      );
  }
}
