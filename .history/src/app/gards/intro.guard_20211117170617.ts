import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
export const INTRO_KEY = "intro-has-seean";
import { StorageService } from 'src/app/service/storage.service';


@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {

  constructor(private router: Router, private Storage: StorageService) {
  }

  async canLoad(): Promise<any> {

    const data =  this.Storage.get(INTRO_KEY);

    if (await data !== true) {
      this.router.navigate(['/intro']);
    }
    return (data === true);
  }
}
