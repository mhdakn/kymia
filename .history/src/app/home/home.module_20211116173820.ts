import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { NavigationComponent } from '../components/navigation/navigation.component';
import { PopulardoctorsPage } from '../pages/populardoctors/populardoctors.page';

import { ConsultationsComponent } from '../component/consultations/consultations.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, NavigationComponent, PopulardoctorsPage, ConsultationsComponent],
  entryComponents: [NavigationComponent, PopulardoctorsPage, ConsultationsComponent]
})
export class HomePageModule {}
