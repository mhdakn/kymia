import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoctorsPageRoutingModule } from './doctors-routing.module';

import { DoctorsPage } from './doctors.page';
import { AlldoctorsComponent } from '../../component/alldoctors/alldoctors.component';
import { DoctorService } from 'src/services/doctor.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoctorsPageRoutingModule
  ],
  declarations: [DoctorsPage, AlldoctorsComponent],
  providers: [{ provide: DoctorService }],
  entryComponents:[AlldoctorsComponent]

})
export class DoctorsPageModule {}
