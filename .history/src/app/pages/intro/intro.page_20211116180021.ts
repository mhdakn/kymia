import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { Router } from '@angular/router';

export 

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  currentIndex = 0;

slides = [
    {
      title: 'Obtenez rapidement un rendez-vous',
      description: 'Recherchez un médecin adéquat selon la catégories et l’expertise puis prenez rendez-vous juste en un clic.',
      image: 'assets/img/intro3.png'
    },
    {
      title: 'Ayez une reponse rapide',
      description: 'Consultez votre médecin traintant ou un autre médecin pour obtenir des conseils médicaux en réduisant le temps passé en salle d’attente et les frais de transport.',
      image: 'assets/img/azerty.png'
    },
    {
      title: 'Rejoignez le réseau professionnel des médecin panafricain',
      description: 'Recherchez un médecin adéquat selon la catégories et l’expertise puis prenez rendez-vous juste en un clic.',
      image: 'assets/img/intro2.png'
    }
  ];
  slideOpts = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: false


  };
  constructor(
    private router: Router,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  start() {
    this.navCtrl.navigateRoot('/login');
  }
  @ViewChild('slideblock', {static: true}) slideblock: IonSlides;

  slideChanged(e: any) {
      this.slideblock.getActiveIndex().then((index: number) => {
        this.currentIndex = index;
      });
  }

  next(e: any){
    this.slideblock.getActiveIndex().then((index: number) => {
      this.currentIndex = index;
    });
    this.slideblock.slideNext();
  }








}
