import { Component, Injector, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginService } from 'src/services/login.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import jwtDecode, * as jwt_decode from 'jwt-decode';
import { INTRO_KEY } from 'src/app/gards/intro';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  router: Router;
  loginService: LoginService;
  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);


   constructor(
    private injector: Injector
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.router = this.injector.get<Router>(Router);
    this.loginService = this.injector.get<LoginService>(LoginService);
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }

goToForgetPwd() {
    this.router.navigate(['/pwdforget']);
  }
goToValidation() {
  this.router.navigate(['/validation'])
}

  onSubmit() {
    this.error.next(null);

      if (!this.submit.getValue()) { this.submit.next(true); }

      if (this.submit.getValue() && !this.loading.getValue()) {
        this.loading.next(true);

       if (this.loginForm.get('email').valid && this.loginForm.get('password').valid ){
         this.loginService.login(
           {
             email: this.loginForm.get('email').value,
             password: this.loginForm.get('password').value
          }
        )
        .subscribe(
          (res: any) => {
            this.loading.next(true);
            this.submit.next(false);

            let token = JSON.parse(JSON.stringify(res.token));
            var user = jwtDecode(token);

            localStorage.token = res.token;
            localStorage.user = JSON.stringify(user);

            console.log(localStorage);
            this.goToHome();
          },
          error => {
            this.loading.next(false);
            this.submit.next(false);
            let errorvalid = error.error.error;
            if( errorvalid === "PENDING_USER"){
              this.goToValidation();
            }else{
            this.error.next(error.error.message);
            }
           // this.message = error.error.message;
            //error.message === environment.login.error ? this.error = environment.static.login.translate.invalid
              //: this.error = environment.static.login.translate.error;
          }
        );
       }
       else{
          this.loading.next(false);

       }
      }

    }



}
