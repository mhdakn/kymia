import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PopoverController, ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { PwdforgetService } from 'src/services/pwdforget.service';
import { PopoverPwdreinitializeComponent } from 'src/app/components/popover-pwdreinitialize/popover-pwdreinitialize.component';

@Component({
  selector: 'app-pwdforget',
  templateUrl: './pwdforget.page.html',
  styleUrls: ['./pwdforget.page.scss'],
})
export class PwdforgetPage implements OnInit {
  pwdforgetForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  router: Router;
  pwdforgetService: PwdforgetService;
  error: any;
  message: any;

  constructor(
    private injector: Injector,
    public popoverController: PopoverController,
    public toastController: ToastController,
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.router = this.injector.get<Router>(Router);
    this.pwdforgetService = this.injector.get<PwdforgetService>(PwdforgetService);
  }

  ngOnInit() {
    this.pwdforgetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.pwdforgetForm.controls;
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  onSubmit(event) {
    this.error = null;

    if (!this.submit.getValue()) { this.submit.next(true); }

    if (this.submit.getValue() && !this.loading.getValue()) {
      this.loading.next(true);

      if (this.pwdforgetForm.get('email').valid) {
        this.pwdforgetService.pwdforget(this.pwdforgetForm.get('email').value)
          .subscribe(
            (res: any) => {
              this.loading.next(false);
              this.submit.next(false);
              this.notify();
            },
            error => {
              this.loading.next(false);
              this.submit.next(false);
              this.error = error.status;
              // this.message = error.error.message;
              //error.message === environment.login.error ? this.error = environment.static.login.translate.invalid
              //: this.error = environment.static.login.translate.error;
            }
          );

      }
      else {
        this.loading.next(false);
      }
    }
  }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverPwdreinitializeComponent,
      cssClass: 'pwdpopover',
      event: ev,
      animated: true,
      backdropDismiss: false,
      showBackdrop: true,
      translucent: true
    });
    await popover.present();

  }

  async notify() {
    const toast = await this.toastController.create({
      message: 'Modifié avec succes',
      duration: 2000,
      color: 'success'
    });
    toast.present();

  }


}
