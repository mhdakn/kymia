import { NgModule, LOCALE_ID } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);
import { IntroGuard } from 'src/app/guards/intro.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabnav/tabnav.module').then(m => m.TabnavPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'intro',
    loadChildren: () => import('../app/pages/intro/intro.module').then(m => m.IntroPageModule),
  },
  {
    path: 'login',
    loadChildren: () => import('../app/pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'pwdforget',
    loadChildren: () => import('../app/pages/pwdforget/pwdforget.module').then(m => m.PwdforgetPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'newpwd',
    loadChildren: () => import('../app/pages/newpwd/newpwd.module').then(m => m.NewpwdPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'taking',
    loadChildren: () => import('./pages/appointment-taking/appointment-taking.module').then(m => m.AppointmentTakingPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {

    path: 'details',
    loadChildren: () => import('./pages/details/details.module').then(m => m.DetailsPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },

  {
    path: 'populardoctors',
    loadChildren: () => import('./pages/populardoctors/populardoctors.module').then(m => m.PopulardoctorsPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'validation',
    loadChildren: () => import('./pages/doublevalidation/doublevalidation.module').then(m => m.DoublevalidationPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'specialities',
    loadChildren: () => import('./pages/specialities/specialities.module').then(m => m.SpecialitiesPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'doctors',
    loadChildren: () => import('./pages/alldoctors/alldoctors.module').then(m => m.AlldoctorsPageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule),
    canLoad: [IntroGuard, AuthGuard]
  },
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'specialists',
    loadChildren: () => import('./pages/specialists/specialists.module').then(m => m.SpecialistsPageModule),
    canLoad: [IntroGuard, AuthGuard]
  }


  // {
  //   path: 'folders',
  //   loadChildren: () => import('./pages/folders/folders.module').then(m => m.FoldersPageModule),
  // },
  // {
  //   path: 'meetings',
  //   loadChildren: () => import('./pages/meetings/meetings.module').then(m => m.MeetingsPageModule)
  // },
  // {
  //   path: 'setting',
  //   loadChildren: () => import('./pages/setting/setting.module').then(m => m.SettingPageModule),
  //   canLoad: [IntroGuard]
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR' },
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
