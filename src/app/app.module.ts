import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicStorageModule } from '@ionic/storage-angular';
import { UserService } from 'src/app/services/user.service';
import { ApiinterceptorInterceptor } from 'src/app/services/apiinterceptor.interceptor';
import { CommonService } from 'src/app/services/common.service';
import { DatePipe } from '@angular/common';
import { AccountService } from 'src/app/services/account.service';
import { LocationService } from 'src/app/services/location/location.service';
import { DoctorService } from "src/app/services/doctor.service";
import { ConsultationsService } from "src/app/services/consultations.service";
import { SharedModule } from './components/shared/shared.module';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [

    IonicStorageModule.forRoot({
      name: '__Kymia'  /// setting local database name
    }),
    BrowserModule,  ReactiveFormsModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule, SharedModule],
  providers: [
    UserService,DoctorService, DatePipe,
    UserService, CommonService, AccountService, LocationService, DoctorService, ConsultationsService,
    { provide: ApiinterceptorInterceptor, useClass: ApiinterceptorInterceptor, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
