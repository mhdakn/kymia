import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-appointment-hours',
  templateUrl: './appointment-hours.component.html',
  styleUrls: ['./appointment-hours.component.scss'],
})
export class AppointmentHoursComponent implements OnInit {
  @Input() item;
  @Input() selected; 

  show: Boolean = false;
  heurrdv:string;

  constructor() { }

  ngOnInit() {
    //console.log(this.item)
    const date = this.item.availableDateStart.split(":")
    this.heurrdv=date[0]+":"+date[1];

  }

  setColor() {
    this.show = !this.show;
  }
  
  show_color(){
    //console.log(this.selected === this.item.id)
    return this.selected === this.item.id;
  }

}


