import { Component, OnInit, Input, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from "@angular/common"; //VOS MODICATIONS

@Component({
  selector: 'app-consultations',
  templateUrl: './consultations.component.html',
  styleUrls: ['./consultations.component.scss'],
})
export class ConsultationsComponent implements OnInit {
  list_consultations : any;
  @Input() item;
  constructor() { };
  async ngOnInit() {
    let month_abbreviation = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
    const dates = this.item.updateAt;
    switch (this.item.status) {
        case 'WAITING':
          this.item.status= "En attente"
          break;
        case 'CONFIRM_BY_DOCTOR':
          this.item.status= "Confirmé"
          break;
        case 'PAYED':
          this.item.status= "Payé"
          break;
        case 'ANNULED':
          this.item.status= "Annulé"
          break;
        case 'TERMINATED':
          this.item.status= "Terminé"
          break;
      }
    this.list_consultations = {
      title: "Dr " + this.item.calendar.doctor.name + " " + this.item.calendar.doctor.firstname.substr(0, 1),
      problemDescription: this.item.problemDescription,
      image: this.item.calendar.doctor.photo,
      date: dates.substr(0, 2) + " " + month_abbreviation[parseInt(dates.substr(3, 2))] + " " + dates.substr(6, 4),
      hours: dates.substr(11, 5),
      status: this.item.status
    }
  }

}
