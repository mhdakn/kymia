import { Component, OnInit, Input, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@Component({
  selector: 'app-doctor-search',
  templateUrl: './doctor-search.component.html',
  styleUrls: ['./doctor-search.component.scss'],
})
export class DoctorSearchComponent implements OnInit {
  @Input() item;

  constructor() { }

  ngOnInit() {
    console.log(this.item)
  }

}
