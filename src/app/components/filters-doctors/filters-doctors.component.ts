import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SlideFilterDoctor } from 'src/app/models/slideFilterDoctors';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-filters-doctors',
  templateUrl: './filters-doctors.component.html',
  styleUrls: ['./filters-doctors.component.scss'],
})
export class FiltersDoctorsComponent implements OnInit {
@Input() item : SlideFilterDoctor = { id: 0, name: ''}; 
@Input() selected; 
@Output() sendItem = new EventEmitter<string>();

private  categoriename: BehaviorSubject<string> = new BehaviorSubject<string>(this.item.name);
public readonly categoryname$: Observable<string> =  this.categoriename.asObservable();

  constructor() { }

  ngOnInit() {
    
  }
  
  setItem() {
    this.categoriename.next(this.item.name);
    this.sendItem.emit(this.item.name);
  }
   show(){
     return this.selected === this.item.id;
   }

}
