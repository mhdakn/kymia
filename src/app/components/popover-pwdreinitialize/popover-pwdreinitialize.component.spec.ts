import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PopoverPwdreinitializeComponent } from './popover-pwdreinitialize.component';

describe('PopoverPwdreinitializeComponent', () => {
  let component: PopoverPwdreinitializeComponent;
  let fixture: ComponentFixture<PopoverPwdreinitializeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverPwdreinitializeComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PopoverPwdreinitializeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
