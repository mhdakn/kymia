import { Component, OnInit, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NavController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-pwdreinitialize',
  templateUrl: './popover-pwdreinitialize.component.html',
  styleUrls: ['./popover-pwdreinitialize.component.scss'],
})
export class PopoverPwdreinitializeComponent implements OnInit {

  constructor(
    public popoverController: PopoverController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {}

  quit(){
    this.popoverController.dismiss();
    this.navCtrl.navigateRoot('/login');
  }

  goToLogin(){
    this.navCtrl.navigateRoot('/login');
  }


}
