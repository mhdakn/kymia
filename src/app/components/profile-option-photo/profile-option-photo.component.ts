import { Component, OnInit, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-profile-option-photo',
  templateUrl: './profile-option-photo.component.html',
  styleUrls: ['./profile-option-photo.component.scss'],
})
export class ProfileOptionPhotoComponent implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }

  ngOnInit() {}

  closeModal(){
    this.modalController.dismiss(null, 'backdrop');
  }

  startCapture(type){
    this.modalController.dismiss(type, 'select');
  }
}
