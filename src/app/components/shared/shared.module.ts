import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentHoursComponent } from '../appointment-hours/appointment-hours.component';
import { ConsultationsComponent } from '../consultations/consultations.component';
import { DoctorSearchComponent } from '../doctor-search/doctor-search.component';
import { DoctorsComponent } from '../doctors/doctors.component';
import { PopoverPwdreinitializeComponent } from '../popover-pwdreinitialize/popover-pwdreinitialize.component';
import { ProfileOptionPhotoComponent } from '../profile-option-photo/profile-option-photo.component';
import { SpecialityComponent } from '../speciality/speciality.component';



@NgModule({
  declarations: [
    AppointmentHoursComponent,
    ConsultationsComponent,
    DoctorSearchComponent,
    DoctorsComponent,

    PopoverPwdreinitializeComponent,
    ProfileOptionPhotoComponent,
    SpecialityComponent
  ],
  exports: [

    AppointmentHoursComponent,
    ConsultationsComponent,
    DoctorSearchComponent,
    DoctorsComponent,

    PopoverPwdreinitializeComponent,
    ProfileOptionPhotoComponent,
    SpecialityComponent
  ],
  imports: [
    CommonModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class SharedModule { }
