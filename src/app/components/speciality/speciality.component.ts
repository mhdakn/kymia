import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.scss'],
})
export class SpecialityComponent implements OnInit {
  @Input() item;
  constructor() { }

  ngOnInit() {}

}
