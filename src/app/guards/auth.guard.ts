import { Injectable } from '@angular/core';
import { CanLoad,Router } from '@angular/router';
import {UserService } from 'src/app/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private router: Router, private User: UserService) {
  }

 async canLoad():  Promise<boolean>  {

   if (!this.User.isLogged) {
      this.router.navigate(['/login']);
    }
   return this.User.isLogged;
  }
}
