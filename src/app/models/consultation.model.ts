export class ConsultationData {
    calendarId: number;
    patientId: number;
    problemDescription: string
}