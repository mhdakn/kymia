export class PaymentProof {
    createAt: string;
    id: number;
    internalName: string;
    lien: string;
    originalName: string;
    personId: number;
    type: string;
    updateAt: string;
}

export class Transaction {
    billingOperator: string;
    billingPhoneNumber: string;
    id: number;
    paymentProof: PaymentProof;
    price: number;
    transactionDate: string;
}

export class Consultation {
    callUrl: string;
    comment: string;
    createAt: string;
    id: number;
    note: number;
    problemDescription: string;
    status: string;
    transaction: Transaction;
    updateAt: string;
}

export class Calendar {
    available: boolean;
    availableDate: string;
    availableDateEnd: string;
    availableDateStart: string;
    consultation: Consultation;
    createAt: string;
    id: number;
    updateAt: string;
}

export class Doctor {
    adresse: string;
    calendars: Calendar[];
    city: string;
    consultationPrice: number;
    country: string;
    createAt: string;
    email: string;
    firstname: string;
    fonction: string;
    id: number;
    name: string;
    office: string;
    password: string;
    phone: string;
    photo: string;
    status: string;
    type: string;
    updateAt: string;
}

export class Doctor_GET {
    adresse: string;
    calendars: Calendar[];
    city: object;
    diplomaDate: string;
    domain: object;
    consultationPrice: number;
    nbrConsult: number;
    createAt: string;
    email: string;
    firstname: string;
    fonction: string;
    id: number;
    name: string;
    office: string;
    password: string;
    phone: string;
    photo: string;
    status: string;
    type: string;
    updateAt: string;
}
