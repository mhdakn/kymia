export class RequestDoctor {
    adresse?: string;
    city?: string;
    country?: string;
    email: string;
    firstname: string;
    fonction: string;
    name: string;
    phone: string;
    password: string;
}
export class DoctorDto {
    adresse: string;
    cityId: number;
    email: string;
    country?: string;
    firstname: string;
    fonction: string;
    name: string;
    phone: string;
    office: string;
}

export class RequestUser {
    birthdate: string;
    city?: string;
    country?: string;
    email: string;
    firstname: string;
    groupeSanguin?: string;
    name: string;
    password: string;
    phone: string;
}

export class RequestCalandar{
        availability : boolean;
        date : string;
        domainId: number;
        getFirst:boolean;
}
export class UserDto {
    birthdate: string;
    cityId: string;
    country?: string;
    email: string;
    firstname: string;
    groupeSanguin: string;
    name: string;
    phone: string;
    genre: string;
}
