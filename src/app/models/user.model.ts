
export class Calendar {
    available: boolean;
    availableDate: string;
    availableDateEnd: string;
    availableDateStart: string;
    createAt: string;
    id: number;
    updateAt: string;
}

export class PaymentProof {
    createAt: string;
    id: number;
    internalName: string;
    lien: string;
    originalName: string;
    personId: number;
    type: string;
    updateAt: string;
}

export class Consultation {
    calendar: Calendar;
    callUrl: string;
    comment: string;
    createAt: string;
    id: number;
    note: number;
    problemDescription: string;
    status: string;
    transaction: Transaction;
    updateAt: string;
}

export class Transaction {
    billingOperator: string;
    billingPhoneNumber: string;
    id: number;
    paymentProof: PaymentProof;
    price: number;
    transactionDate: string;
}

export class User {
    birthdate: string;
    city: string;
    consultations: Consultation[];
    country: string;
    createAt: string;
    email: string;
    firstname: string;
    groupeSanguin: string;
    id: number;
    name: string;
    password: string;
    phone: string;
    photo: string;
    status: string;
    transactions: Transaction[];
    type: string;
    updateAt: string;
}
