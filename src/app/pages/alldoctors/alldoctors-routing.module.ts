import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlldoctorsPage } from './alldoctors.page';

const routes: Routes = [
  {
    path: '',
    component: AlldoctorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlldoctorsPageRoutingModule {}
