import { AutocompleteLibModule } from '../../modules/autocomplete-lib/src/lib/autocomplete-lib.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';
import {Ng2SearchPipeModule} from 'ng2-search-filter';

import { AlldoctorsPage } from './alldoctors.page';
import { AlldoctorsPageRoutingModule } from './alldoctors-routing.module';
import { FiltersDoctorsComponent } from '../../components/filters-doctors/filters-doctors.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    AlldoctorsPageRoutingModule,
    Ng2SearchPipeModule,
    AutocompleteLibModule
  ],
  providers: [ ],
  declarations: [AlldoctorsPage,FiltersDoctorsComponent],
  entryComponents:[FiltersDoctorsComponent]

})
export class AlldoctorsPageModule { }
