import { Component, OnInit, Injector } from '@angular/core';
import { DoctorService } from 'src/app/services/doctor.service';
import { NavController } from '@ionic/angular';
import { CommonService } from 'src/app/services/common.service';


@Component({
  selector: 'app-alldoctors',
  templateUrl: './alldoctors.page.html',
  styleUrls: ['./alldoctors.page.scss'],
})
export class AlldoctorsPage implements OnInit {
  item_all = [
    {
      id: 0,
      name: "Tous"
    }
  ];
  items_doctors: any = [];
  item_activities: any = [];
  searchs: any = [];
  listflter: any = [];
  selected = 0;
  items: string;
  status = false;
  searchTerm: string;

  nav: NavController;
  doctorService: DoctorService;
  commonService: CommonService;

  slideOpts = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 3,
    dots: false,
    point: false,
    autoplay: false
  };

  constructor(
    private injector: Injector
  ) {
    this.nav = this.injector.get<NavController>(NavController);
    this.doctorService = this.injector.get<DoctorService>(DoctorService);
    this.commonService = this.injector.get<CommonService>(CommonService);
  }

  ngOnInit() {
    this.menuDoctors();
    this.menuActivities();
  }

  selectEvent() {
    this.status = !this.status;
  }

  getItemTitle(item: any, i: any) {
    i++;
    if (this.searchs.indexOf(item.name.toLowerCase()) > -1) {
      this.searchs = this.searchs.filter(e => e !== item.name.toLocaleLowerCase())
    } else {
      this.searchs.pop();
      this.searchs.push(item.name.toLowerCase())
    }
    if (this.selected === i) {
      this.selected = null;
      //filter of doctors
      return this.listflter = (this.searchs.length == 0) ? this.items_doctors : this.items_doctors.filter(({ domain }) => this.searchs.includes(domain.name.toLowerCase()));
    }
    else if (this.selected != i) {
      this.selected = i;
      //filter of doctors 
      return this.listflter = (this.searchs.length == 0) ? this.items_doctors : this.items_doctors.filter(({ domain }) => this.searchs.includes(domain.name.toLowerCase()));
    }
  }

  menuDoctors() {
    this.doctorService.getAllDoctors().subscribe(
      (res: any) => {
        this.items_doctors = res.content;
        this.listflter = res.content;
        return this.items_doctors;
      }
    )
  }

  allDoctors(item, i) {
    this.selected = i;
    if (this.selected === i) {
      this.selected = 0;
    }
    else if (this.selected != i) {
      this.selected = 0;
    }
    return this.listflter =  this.items_doctors;
  }

  menuActivities() {
    this.commonService.getActivities().subscribe(
      (data: any) => {
        this.item_activities = data.content;
        return this.item_activities;
      })
  }
  gotoDetails(user) {
    this.nav.navigateRoot(['details'], { state: { doctorID: user.id } });
  }
}
