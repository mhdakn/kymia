import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AppointmentTakingPageRoutingModule } from './appointment-taking-routing.module';


import { AppointmentTakingPage } from './appointment-taking.page';
import { DoctorSearchComponent } from '../../components/doctor-search/doctor-search.component';
import { SharedModule } from '../../components/shared/shared.module';
import { AppointmentHoursComponent } from "../../components/appointment-hours/appointment-hours.component";
import { DatePipe} from "@angular/common";



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AppointmentTakingPageRoutingModule,
    SharedModule
  ],
  providers: [DatePipe],
  declarations: [AppointmentTakingPage]
})
export class AppointmentTakingPageModule {}
