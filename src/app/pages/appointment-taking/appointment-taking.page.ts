import { Component, OnInit, Injector } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DoctorService } from "src/app/services/doctor.service";
import { UserService } from "src/app/services/user.service";
import { CommonService } from "src/app/services/common.service";
import { Doctor_GET } from "../../models/doctor.model";
import { NavController } from '@ionic/angular';
import { DatePipe } from "@angular/common";

// import {formatDate} from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-appointment-taking',
  templateUrl: './appointment-taking.page.html',
  styleUrls: ['./appointment-taking.page.scss'],
})
export class AppointmentTakingPage implements OnInit {
  datechoice: string;
  empty: boolean = false;
  showDoctor: boolean = false;
  doctoDispo: Boolean = false;
  usedoctor: Boolean = true;
  doctorNAME: string;
  today: string;
  calandars: any = [];
  calandar_id: number = 0;
  user: any;
  problemDescription: string = "";
  meetingHS: any = [];
  domaineName: any = [];
  domaine: any = [];
  domaineID: number;
  doctor: any = [];
  domaneChoice: number;
  hoursChoice: string;
  idDoctor: number;
  show: boolean = false;
  showDomaine: boolean = true;
  showHoraire: boolean = false;
  doctor_data: Doctor_GET;
  selected:any;

  nav: any;

  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    public readdoctors: DoctorService,
    public datePipe: DatePipe,
    private router: Router,
    public consultationService: UserService,
    public calandarservice: UserService,
    public commonservice: CommonService,
    private injector: Injector) {
    this.nav = this.injector.get<NavController>(NavController);

  }


  ngOnInit() {
    this.addDays(3);
    this.datechoice = this.today;
   
    // recuperation de tous les domaines.

    this.commonservice.getActivities().subscribe(
      (data: any) => {
        const list = data.content;

        list.map(e => {
          this.domaine.push(e)

        })
      },
      (error: any) => {
        // console.log(error);
      }

    );

    // vérifier si on quitte la page docteur pour venir a cette page
    try {
      this.idDoctor = this.router.getCurrentNavigation().extras.state.p1;
      this.showDomaine = this.router.getCurrentNavigation().extras.state.p2;
      this.usedoctor = this.router.getCurrentNavigation().extras.state.p3;
      this.showHoraire = this.router.getCurrentNavigation().extras.state.p4;
      this.domaneChoice = this.router.getCurrentNavigation().extras.state.id;

    } catch (error) {

    }

    if (this.idDoctor) {
      this.readdoctors.getDoctorCalandar(this.idDoctor).subscribe(
        (data: any) => {
          this.calandars = data.content;
          this.setDate();
        },
        (error: any) => {
        }
      )
    }



  



  }



  // ionViewWillEnter(){
  //   console.log("le will enter est le 1er")
  // }




  addDays(days: number): Date {
    var futureDate = new Date();
    futureDate.setDate(futureDate.getDate() + days);
    this.today = futureDate.toJSON().split('T')[0];
    return futureDate;
  }

  setTime(value,i) {
    this.calandar_id = value.id

    this.show = !this.show;
    if(value.id===this.meetingHS[i].id){

      if (this.selected === this.meetingHS[i].id) {
        this.selected = null;
      
      } 
      else if (this.selected != this.meetingHS[i].id) {
        this.selected = this.meetingHS[i].id;
     
      }

    }

    
    
  }

  setDate() {
    this.meetingHS = []


    this.calandars.map(e => {
      
      (e.availableDate == this.datePipe.transform(this.datechoice, "dd/MM/yyyy")) ? this.meetingHS.push(e) : this.empty = true;

      return (this.meetingHS.length > 0) ? this.empty = false : this.empty = true;



    })

    var date = this.datePipe.transform(this.datechoice, "dd/MM/yyyy")

    if (this.domaneChoice) {
      console.log("eya")
      let searchCalandar = {
        availability: true,
        date: date,
        domainId: this.domaneChoice,
        getFirst: true,

      }

      this.doctorNAME = "";
      this.setCalandar(searchCalandar)

    }
  }




  setCalandar(searchCalandar) {
    this.calandarservice.calandarSearch(searchCalandar).subscribe(
      (res: any) => {
        //this.loading.next(false);
        var content = res.content

        if (res.content.length == 0) {
          this.doctor = []
          this.doctoDispo = true;
          this.doctorNAME = "";
        } else {
          this.doctor = []
          content.map(e => {
            this.doctor.push(e)
            this.doctoDispo = false
          })
        }
      })
  }



  setMotif() {
  }

  setClandarid(value) {
    this.calandar_id = value.id
    this.doctorNAME = value.doctor.name + " " + value.doctor.firstname;
    this.showDoctor = false;
  }

  onSubmit() {
    this.user = this.consultationService.user;
    this.loading.next(true);

    let consultation = {
      calendarId: this.calandar_id,
      patientId: this.user.userId,
      problemDescription: this.problemDescription
    }


    this.consultationService.registerConsultation(consultation).subscribe(
      (res: any) => {
        this.loading.next(false);
      })

    this.loading.next(false);
    this.nav.navigateRoot(['tabs/home']);
  }

  setDomaine() {
    this.datechoice = this.today;
    this.setDate()
    this.doctorNAME = "";
    this.doctor = [];
  }

  setDoctor() {
    this.showDoctor = true
  }

  gotoBack() {
    //this.nav.navigateRoot(['specialists']);
    this.nav.pop();
  }

}
