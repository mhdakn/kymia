import { Component, OnInit, Injectable, Injector } from '@angular/core';
import { DoctorService } from "src/app/services/doctor.service";
import { ConsultationsService } from "src/app/services/consultations.service";
import { Doctor_GET } from "../../models/doctor.model";
import { Consultations_Doctor_And_Patients } from "../../models/consultations.model";
import { NavController } from '@ionic/angular';
import { DatePipe } from "@angular/common";
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  @Injectable({
    providedIn: 'root'
  })
  isReadMore = true;
  myParam: any;

  meetings = [
    // {
    //   day_letter: 'MER',
    //   day_number: '20',
    //   month: 'OCT',
    //   type: 'Contrôle cardiaque',
    //   hours: '09 : 00',
    // },
    // {
    //   day_letter: 'MER',
    //   day_number: '20',
    //   month: 'OCT',
    //   type: 'Contrôle cardiaque',
    //   hours: '09 : 00',
    // },
    // {
    //   day_letter: 'VEN',
    //   day_number: '19',
    //   month: 'NOV',
    //   type: 'Contro',
    //   hours: '09 : 00',
    // },
  ];
  rates = [
    {
      'label': 5,
      'percent': '80%'
    },
    {
      'label': 4,
      'percent': '30%'
    },
    {
      'label': 3,
      'percent': '5%'
    },
    {
      'label': 2,
      'percent': '5%'
    },
    {
      'label': 1,
      'percent': '5%'
    },
  ];
  comments = [
    {
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris volutpat tristique tellus, id mollis mi malesuada vitae. Donec tempus convallis aliquam. Sed imperdiet, nulla eu tempus vehicula,leo orci lobortis erat, at mollis dui lacus quis mi.",
      "author": "Joe Smith",
    },
    {
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris volutpat tristique tellus, id mollis mi malesuada vitae. Donec tempus convallis aliquam. Sed imperdiet, nulla eu tempus vehicula,leo orci lobortis erat, at mollis dui lacus quis mi.",
      "author": "Joe Smith",
    },
  ];
  show: string = "about";
  rate: number = 4.5;
  number_consult: number = 4.5;
  doctor_data: any;
  doctorID: number;
  nav: any;
  consultations_data: Consultations_Doctor_And_Patients;
  User: UserService;


  showthis(section) {
    this.show = section
    // console.log(this.show)
  }
  constructor(
    public readdoctors: DoctorService,
    private injector: Injector,
    private datePipe: DatePipe,
    private router: Router,
    public readconsultations: ConsultationsService,


  ) {
    this.nav = this.injector.get<NavController>(NavController);
    this.User = this.injector.get<UserService>(UserService);
  }

  ngOnInit() {
    try {
      this.doctorID = this.router.getCurrentNavigation().extras.state.doctorID;
      //console.log(this.doctorID)
    } catch (error) {
      //console.log(this.doctorID)
    }

    //console.log(this.doctor_data)
    //console.log("The local storage is : "+localStorage.user)
    //this.doctorID = this.router.getCurrentNavigation().extras.state.doctorID;

    this.readdoctors.getDoctor(this.doctorID).subscribe(
      (data: any) => {
        this.doctor_data = data;
        //console.log("DATA get doctor ", this.doctor_data)
      },
      (error: any) => {
        //console.log("ERROR get doctor",error);
      }
    )
    const user = this.User.user;
    const userId = user?.userId;
    //console.log("ID DU DOCTEUR EST TEST 1",userId )
    this.readconsultations.getAllByDoctorAndPatients(18, userId).subscribe(
      (data: any) => {
        const contents = data.content;
        //console.log("La liste des consultations est : ",contents)
        // contents = [
        //     {
        //       calendar: {
        //         createAt: "20/10/2021 09:00:ss",
        //       },
        //       problemDescription: "Contrôle cardiaque",
        //     },
        //     {
        //       calendar: {
        //         createAt: "19/11/2021 09:40:ss",
        //       },
        //       problemDescription: "Contro",
        //     },
        //     {
        //       calendar: {
        //         createAt: "25/11/2021 09:15:ss",
        //       },
        //       problemDescription: "Contro",
        //     },
        // ]
        let consultations_length = contents.length
        let month_abbreviation = ["JANV", "FEV", "MARS", "AVR", "MAI", "JUIN", "JUIL", "AOUT", "SEPT", "OCT", "NOV", "DEC"]
        let day_abbreviation = ["DIM", "LUN", "MAR", "MER", "JEU", "VEN", "SAM"]
        for (let index = 0; index < consultations_length; index++) {
          this.consultations_data = contents[index];
          if (typeof (this.consultations_data) !== "undefined") {
            let calendar = this.consultations_data.calendar.createAt
            if (typeof (calendar) !== "undefined") {
              let indice = parseInt(calendar.substr(3, 2), 10) - 1 // Because indice starts by 0
              let month_value = month_abbreviation[indice]
              let date_english_format = month_value + "/" + calendar.substr(0, 2) + "/" + calendar.substr(6, 4)
              var d = new Date(date_english_format);
              var dayName = day_abbreviation[d.getDay()];
              this.meetings.push(
                {
                  day_letter: dayName,
                  day_number: calendar.substr(0, 2), //substr is the starting index and the number of characters after it
                  month: month_value,
                  type: this.consultations_data.problemDescription,
                  hours: calendar.substr(11, 5),
                },
              )
            }
          }
        }
      },
      (error: any) => {
        //console.log("ERROR",error);
      }
    )
  }
  returnHome() {
    this.nav.navigateRoot(['tabs/home'])
  }
  appointment() {
    //console.log(this.readdoctors.current_doctor_id)
    this.readdoctors.getDoctorID();
    //this.router.navigateByUrl('/taking');
    this.router.navigate(['/taking'], { state: { p1: this.readdoctors.current_doctor_id, p2: false, p3: false, p4: true } });

    //this.nav.navigateRoot(['/taking'], { state: { p1: this.readdoctors.current_doctor_id, p2: false, p3: false, p4: true } });
  }
 /*
 * function for read more or less
 */
  showText() {
    this.isReadMore = !this.isReadMore
 }

}
