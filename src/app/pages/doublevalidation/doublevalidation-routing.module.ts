import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoublevalidationPage } from './doublevalidation.page';

const routes: Routes = [
  {
    path: '',
    component: DoublevalidationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DoublevalidationPageRoutingModule {}
