import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DoublevalidationPageRoutingModule } from './doublevalidation-routing.module';

import { DoublevalidationPage } from './doublevalidation.page';
// import { ValidationService } from 'src/services/validation.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DoublevalidationPageRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  declarations: [DoublevalidationPage]
})
export class DoublevalidationPageModule {}
