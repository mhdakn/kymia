import { StorageService } from 'src/app/services/storage.service';
import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { UserService as ValidationService } from 'src/app/services/user.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-doublevalidation',
  templateUrl: './doublevalidation.page.html',
  styleUrls: ['./doublevalidation.page.scss'],
})
export class DoublevalidationPage implements OnInit {
  validationForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  router: Router;
  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  validationService: ValidationService;
  loginService: UserService;


  constructor(
    private injector: Injector,
    private storage: StorageService,
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.router = this.injector.get<Router>(Router);
    this.validationService = this.injector.get<UserService>(UserService);
    this.loginService = this.injector.get<UserService>(UserService);
  }

  ngOnInit() {
    this.validationForm = this.formBuilder.group({
      email: ['', Validators.required],
      codeConfirm: ['', Validators.required],
    });
  }
  goToHome() {
    this.router.navigate(['/home']);
  }
  goToLogin() {
    this.router.navigate(['/login']);
  }
  goToValidation() {
    this.router.navigate(['/validation']);
  }
  resendconfirmation() {
    this.validationService.resend(localStorage.email).subscribe(
      (res: any) => {
        //this.loading.next(true);
        this.submit.next(false);

        this.storage.set('token', res.token);
      },
      error => {
        this.loading.next(false);
        this.submit.next(false);
        let errorvalid = error.error.error;

        this.error.next(error.error.message);
      }

    );

  }

  connection() {
    //this.router.navigate(['/login']);
    let role = localStorage.type;
    if (role === "Patient") {

      this.loginService.login(
        {
          email: localStorage.email,
          password: localStorage.password,
        }
      )

        .subscribe(
          (res: any) => {
            this.loading.next(true);
            this.submit.next(false);

            this.storage.set('token', res.token);
            this.goToHome();
          },
          error => {
            this.loading.next(false);
            this.submit.next(false);
            let errorvalid = error.error.error;

            this.error.next(error.error.message);
          }
        );
    } else {
      this.goToLogin();
    }

  }

  get f(): { [key: string]: AbstractControl } {
    return this.validationForm.controls;
  }

  onSubmit() {
    this.error.next(null);
    const email = localStorage.email;
    if (!this.submit.getValue()) { this.submit.next(true); }

    if (this.submit.getValue() && !this.loading.getValue()) {
      this.loading.next(true);

      if (this.validationForm.get('codeConfirm').valid) {
        this.validationService.validation(
          {
            email,
            code: this.validationForm.get('codeConfirm').value
          }
        )
          .subscribe(
            (res: any) => {
              this.loading.next(true);
              this.submit.next(false);
              this.connection();
              localStorage.clear();
            },
            error => {
              this.loading.next(false);
              this.submit.next(false);
              this.error.next(error.error.message);
              // this.message = error.error.message;
              //error.message === environment.login.error ? this.error = environment.static.login.translate.invalid
              //: this.error = environment.static.login.translate.error;
            }
          );
      }
      else {
        this.loading.next(false);

      }
    }

  }

}
