import { HomePage } from './home.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {Ng2SearchPipeModule} from 'ng2-search-filter';


import { HomePageRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/components/shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HomePageRoutingModule,
    SharedModule,
    Ng2SearchPipeModule
  ],
  providers: [],
  declarations: [HomePage],
  entryComponents : [],

})
export class HomePageModule { }
