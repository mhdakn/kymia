import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { HistoryService } from 'src/app/services/history.service';
import { UserService } from '../../../app/services/user.service';
import { DoctorService } from 'src/app/services/doctor.service';
import {CommonService} from 'src/app/services/common.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  searchTerm:string;
  item_example={
    title: 'Dr Ryan Mab',
    problemDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget nulla.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget nulla.',
    image: '../../assets/img/azerty.png',
    date: '24 Octobre 2021',
    hours: '09:00',
    status: 'Confirmé'
  }
  themes = [
    {
      src: '../../assets/img/home/generalist.png',
      name: 'Généraliste',
      alt: 'Generaliste'
    },
    {
      src: '../../assets/img/home/veterinarian.png',
      name: 'Vétérinaire',
      alt: 'Veterinaire'
    },
    {
      src: '../../assets/img/home/pediatrician.png',
      name: 'Pédiatre',
      alt: 'Pediatre'
    },
    {
      src: '../../assets/img/home/dentist.png',
      name: 'Dentiste',
      alt: 'Dentiste'
    },
    {
      src: '../../assets/img/home/generalist.png',
      name: 'Généraliste',
      alt: 'Generaliste'
    },
    {
      src: '../../assets/img/home/veterinarian.png',
      name: 'Vétérinaire',
      alt: 'Veterinaire'
    },
    {
      src: '../../assets/img/home/pediatrician.png',
      name: 'Pédiatre',
      alt: 'Pediatre'
    },
    {
      src: '../../assets/img/home/dentist.png',
      name: 'Dentiste',
      alt: 'Dentiste'
    }
  ];
  user : any;
  firstname: string;
  lastname: string;
  historyService: HistoryService;
  list = [];
  currentIndex = 0;
  slideOpts = {
    initialSlide: 0,
    speed: 1000,
    slidesPerView: 1,
    autoplay: true,
    effect: 'fade',
  };
  formBuilder: any;
  nav: any;
  doctorService: any;
  doctors_list =  [];
  items: any = [];

  constructor(
    private User: UserService,
    private storage: StorageService,
    private injector: Injector,
    public commonService: CommonService
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.nav = this.injector.get<NavController>(NavController);
    this.historyService = this.injector.get<HistoryService>(HistoryService);
    this.doctorService = this.injector.get<DoctorService>(DoctorService)
  };
  async ngOnInit() {
    this.storage.init();
    const user = this.User.user;
   
    //get user lastname and firstname
    if(user?.role === "PATIENT"){
      this.firstname = user?.firstname;
      this.lastname = user?.lastname;
    }else if(user?.role === 'DOCTOR'){
      this.firstname = user?.firstname;
      this.lastname = user?.lastname;
    }

    // get user id
    const userId = user?.userId;
    let month_abbreviation = ["Janvier","Février", "Mars", "Avril","Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
    this.historyService.getConsultation(userId).subscribe(
      (res: any) => {
        this.list = res.content;
      }
    )
    this.doctorService.getAllDoctors().subscribe(
      (res: any) => {
        this.doctors_list = res.content;
        return this.doctors_list;
      }
    )

    this.commonService.getActivities().subscribe(
      (data:any) =>{
        const list = data.content;

        for (let index = 0; index < list.length; index++) {
          const element = list[index];
          this.items.push(
            {
              id: element.id,
              images: element.icon,
              title: element.name
            }
          )
        }
      }
    )

    this.commonService.getActivities().subscribe(
      (data:any) =>{
        const list = data.content;
        for (let index = 0; index < list.length; index++) {
          const element = list[index];
          this.items.push(
            {
              id: element.id,
              images: element.icon,
              title: element.name,
              description: element.description,
            }
          );
          
        }
      }
    )
  }

  


  slideNext() {
    if (this.currentIndex === 2) {
      this.currentIndex = 0;
    }
    this.currentIndex++;
  };
  
  goToMeetings(){
    this.nav.navigateRoot('/tabs/meetings');
  }

  goToAlldoctors(){
    this.nav.navigateRoot('doctors');
  }

  gotoSpecialities(){
    this.nav.navigateRoot('/specialities');

    
  }

  gotoDetails(doctor){
    //console.log(doctor);
    this.nav.navigateRoot(['details'], {state:{doctorID: doctor.id}});

    //this.nav.navigateRoot('/details'), {state:{p1: item.images,p2: item.title,p3:item.id}});
  }

  setSpecialities(item){
    this.nav.navigateRoot(['specialists'], {state:{p3:item.id}});
    console.log(item);
    
  }


}
