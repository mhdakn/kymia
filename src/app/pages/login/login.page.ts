import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BehaviorSubject } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import jwtDecode, * as jwt_decode from 'jwt-decode';
import { INTRO_KEY } from 'src/app/guards/intro.guard';
import { StorageService } from 'src/app/services/storage.service';
import { ApiinterceptorInterceptor } from 'src/app/services/apiinterceptor.interceptor';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  nav: NavController;

  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);


  constructor(
    private injector: Injector,
    private storage: StorageService,
    private loginService: UserService,
    private AuthService : ApiinterceptorInterceptor
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.nav = this.injector.get<NavController>(NavController);
    //  this.loginService = this.injector.get<LoginService>(LoginService);
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
    this.storage.init();
    this.storage.set(INTRO_KEY, true);
  }

  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  goToHome() {
    this.nav.navigateRoot(['tabs/home']);
  }

  goToRegister() {
    this.nav.navigateRoot(['/register']);
  }

  goToForgetPwd() {
    this.nav.navigateRoot(['/pwdforget']);
  }
  goToValidation() {
    this.nav.navigateRoot(['/validation'])
  }

  onSubmit() {
    this.error.next(null);

    if (!this.submit.getValue()) { this.submit.next(true); }

    if (this.submit.getValue() && !this.loading.getValue()) {
      this.loading.next(true);

      if (this.loginForm.get('email').valid && this.loginForm.get('password').valid) {
        this.loginService.login(
          {
            email: this.loginForm.get('email').value,
            password: this.loginForm.get('password').value
          }
        )
          .subscribe(
            (res: any) => {
              this.loading.next(true);
              this.submit.next(false);
              this.loginService.isLogged = true;
              this.loginService.token = res.token;
              this.AuthService.token = res.token;  // Better use of
              try {
                this.loginService.user = jwtDecode(res.token)
              } catch (e) {
                this.loginService.user = {
                  firstname: 'John',
                  surname : 'Doe'
                }
              }

              this.goToHome();
            },
            error => {
              this.loading.next(false);
              this.submit.next(false);
              let errorvalid = error.error.error;
              if (errorvalid === "PENDING_USER") {
                this.goToValidation();
              } else {
                this.error.next(error.error.message);
              }
              // this.message = error.error.message;
              //error.message === environment.login.error ? this.error = environment.static.login.translate.invalid
              //: this.error = environment.static.login.translate.error;
            }
          );
      }
      else {
        this.loading.next(false);
      }
    }

  }



}
