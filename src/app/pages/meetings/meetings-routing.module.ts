import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MeetingsPage } from './meetings.page';

const routes: Routes = [
  {
    path: '',
    component: MeetingsPage
  },
  {
    path: 'meetings',
    loadChildren: () => import('../meetings/meetings.module').then( m => m.MeetingsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MeetingsPageRoutingModule {}
