import { MeetingsPage } from './meetings.page';
import { AutocompleteLibModule } from '../../modules/autocomplete-lib/src/lib/autocomplete-lib.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MeetingsPageRoutingModule } from './meetings-routing.module';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/components/shared/shared.module';

// import { ProfilPageRoutingModule } from './meetings-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    MeetingsPageRoutingModule,
    AutocompleteLibModule
  ],
  providers: [ ],
  declarations: [MeetingsPage]
})
export class MeetingsPageModule { }
