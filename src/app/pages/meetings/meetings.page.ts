import { StorageService } from 'src/app/services/storage.service';
import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from "src/app/services/user.service";
import { NavController,LoadingController, } from '@ionic/angular';
import { HistoryService } from 'src/app/services/history.service';

@Component({
  selector: 'app-meetings',
  templateUrl: './meetings.page.html',
  styleUrls: ['./meetings.page.scss'],
})
export class MeetingsPage implements OnInit {
  historyService: HistoryService;
  listFiltre: any[];
  list: any[];
  default:string
  status: any;
  show: boolean;
  formBuilder: FormBuilder;
  nav: NavController;
  constructor(
    //private User: UserService,
    private router: Router,
    private injector: Injector,
    private storage: StorageService,
    public userservice: UserService
  ) {
    this.default = "next";
    //this.ngOnInit();
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.nav = this.injector.get<NavController>(NavController);
    this.historyService = this.injector.get<HistoryService>(HistoryService);
  }

  async ngOnInit() {
    const user = this.userservice.user;
    const userId = user?.userId;

    this.historyService.getConsultation(userId).subscribe((res: any) => {
      this.list = res.content;
      this.showNext();
      

      return this.list;
    });


  }


  _notNull(data) {
    return (
      data.length > 0 &&
      data.map((e) => {
        return e.status !== '';
      })
    );
  }

  showComplete() {
    this.listFiltre = this.list.filter((e) => {
      return e.status === 'TERMINATED' || e.status === 'Terminé';
    });

    this.listFiltre.length == 0 ? (this.show = true) : (this.show = false);
  }

  showNext() {
    this.listFiltre = this.list.filter(
      (e) =>
        e.status == 'WAITING' ||
        e.status === 'PAYED' ||
        e.status === 'CONFIRM_BY_DOCTOR' ||
        e.status === 'En attente' ||
        e.status === 'Confirmé' ||
        e.status === 'Payé'
    );

    this.listFiltre.length == 0 ? (this.show = true) : (this.show = false);
  }

  takeNewConsultation() {
    this.router.navigate(['/taking']);
  }

  cancelled() {
    this.listFiltre = this.list.filter((e) => {
      return e.status === 'ANNULED' || e.status === 'Annulé';
    });
    this.listFiltre.length == 0 ? (this.show = true) : (this.show = false);
  }

  segmentChanged(ev: any) {
    if (ev.detail.value == 'next') {
      this.showNext();
    }
    if (ev.detail.value == 'completed') {
      this.showComplete();
    }
    if (ev.detail.value == 'cancelled') {
      this.cancelled();
    }
  }
}
