import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewpwdPageRoutingModule } from './newpwd-routing.module';

import { NewpwdPage } from './newpwd.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewpwdPageRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  declarations: [NewpwdPage]
})
export class NewpwdPageModule {}
