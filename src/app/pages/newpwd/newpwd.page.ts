import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/models/user.model';
import {UserService as NewPwdService } from 'src/app/services/user.service';
import { MustMatch} from './MustMatch.validator';
import { ToastController  } from '@ionic/angular';


@Component({
  selector: 'app-newpwd',
  templateUrl: './newpwd.page.html',
  styleUrls: ['./newpwd.page.scss'],
})
export class NewpwdPage implements OnInit {
  newpwdForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  router: Router;
  newpwdService: NewPwdService;
  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  message: any;
  nav: any;



  constructor(
    private injector: Injector,
    private toastController: ToastController
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.router = this.injector.get<Router>(Router);
    this.newpwdService = this.injector.get<NewPwdService>(NewPwdService);
   }


  ngOnInit() {
    this.newpwdForm = this.formBuilder.group({
      oldPassword: [
        '',
        Validators.compose([
          Validators.minLength(8),
          Validators.required
        ])
      ],
      newPassword: [
        '',
        Validators.compose([
          Validators.minLength(8),
          Validators.required
        ])
      ],
      confirmPassword: [
        '',
        Validators.compose([
          Validators.minLength(8),
          Validators.required
        ])
      ],
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.newpwdForm.controls;
  }


  /*
  * go to login page
  */
  goToLogin(): void {
    this.nav.navigateForward('/login');
  }

  /*
  *  go to the home page
  */
  goToHome(): void {
    this.router.navigate(['tabs/home']);
  }

  onSubmit() {
    // console.log(localStorage.user);
    const user = this.newpwdService.user;
    const userId = user?.userId;
    console.log(userId);
    console.log(user);
    
    this.error.next(null);
      if (!this.submit.getValue()) { this.submit.next(true); }
      if (this.submit.getValue() && !this.loading.getValue()) {
        this.loading.next(true);
       if (this.newpwdForm.get('oldPassword').valid && this.newpwdForm.get('newPassword').valid){
         this.newpwdService.newpwd(
            userId,
            {
              oldPassword: this.newpwdForm.get('oldPassword').value,
              newPassword: this.newpwdForm.get('newPassword').value,
            }
         )
         .subscribe(
          (res: any) => {
            this.loading.next(false);
            this.submit.next(false);
            this.notify();
          },
          error => {
            this.loading.next(false);
            this.submit.next(false);
            this.error.next(error.error.message);
           // this.message = error.error.message;
            //error.message === environment.login.error ? this.error = environment.static.login.translate.invalid
              //: this.error = environment.static.login.translate.error;
          }
        );
       }
       else{
          this.loading.next(false);

       }
      }

    }
    async notify() {
      const toast = await this.toastController.create({
        message: 'Modifié avec succes',
        duration: 2000,
        color: 'success'
      });
      toast.present();

    }

}
