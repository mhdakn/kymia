import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopulardoctorsPage } from './populardoctors.page';

const routes: Routes = [
  {
    path: '',
    component: PopulardoctorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopulardoctorsPageRoutingModule {}
