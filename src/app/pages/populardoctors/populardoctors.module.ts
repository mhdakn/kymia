import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopulardoctorsPageRoutingModule } from './populardoctors-routing.module';

import { PopulardoctorsPage } from './populardoctors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopulardoctorsPageRoutingModule
  ],
  declarations: [PopulardoctorsPage]
})
export class PopulardoctorsPageModule {}
