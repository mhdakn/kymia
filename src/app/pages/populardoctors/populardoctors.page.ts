import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-populardoctors',
  templateUrl: './populardoctors.page.html',
  styleUrls: ['./populardoctors.page.scss'],
})
export class PopulardoctorsPage implements OnInit {
  medecins_popular =[
    {
      image: '../../assets/img/home/james_cold.png',
      name: 'Dr James Cold',
      profession: 'Cardiologue au CHNU',
      rate: 4.5,
      comments: 50,
      
    },
    {
      image: '../../assets/img/home/irina_james.png',
      name: 'Dr Irina James',
      profession: 'Pédiatre à la Marténité Lagune',
      rate: 4.5,
      comments: 50,
    },
    {
      image: '../../assets/img/home/ryan_mab.png',
      name: 'Dr Ryan Mab',
      profession: 'Chirugien au CHU',
      rate: 4.5,
      comments: 50,
    },
    {
      image: '../../assets/img/home/raissa_walter.png',
      name: 'Dr Raïssa Walter',
      profession: 'Chirugien au CHU',
      rate: 4.5,
      comments: 50,
    },
  ];
  nav:any;
  constructor(
    private router: Router,
    public navCtrl: NavController,


  ) { }

  ngOnInit() {
  }

  goDetail(){
    //this.router.navigate(['/details'])
    // console.log("toto")
    //this.navCtrl.navigateRoot('/details');
    this.navCtrl.navigateForward('/details');


  }

}
