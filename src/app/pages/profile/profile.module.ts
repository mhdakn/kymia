import { ProfilePage } from './profile.page';
import { AutocompleteLibModule } from '../../modules/autocomplete-lib/src/lib/autocomplete-lib.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilPageRoutingModule } from './profile-routing.module';



import { DatePipe } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ProfilPageRoutingModule,
    AutocompleteLibModule
  ],
  providers: [ DatePipe],
  declarations: [ProfilePage]
})
export class ProfilePageModule { }
