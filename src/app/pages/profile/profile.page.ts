import { ProfileOptionPhotoComponent } from './../../components/profile-option-photo/profile-option-photo.component';
import { City } from '../../models/city.model';
import { Country } from '../../models/country.model';
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-trailing-spaces */
import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController, ModalController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { LocationService } from 'src/app/services/location/location.service';
import { AccountService } from 'src/app/services/account.service';
import { CommonService } from 'src/app/services/common.service';
import { DatePipe } from '@angular/common';
import { UserService } from 'src/app/services/user.service';

//import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
//const { Camera } = Plugins;

@Component({
  selector: 'app-profil',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  photo = 'https://i.pravatar.cc/150';
  status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  search = 'name';
  switch = false;
  readonly = true;
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  activities: any[] = [];
  countries: Country[];
  city: any;
  selectedCountry: Country;
  cities: City[];
  selectedCity: City;


  profilForm = new FormGroup(
    {
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      birthdate: new FormControl('', Validators.required),
      sexe: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.email,
        Validators.required
      ])),
      phone: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      fonction: new FormControl('', Validators.required),
      groupeSanguin: new FormControl('', Validators.required),
      office: new FormControl('', Validators.required),
      adresse: new FormControl('', Validators.required),
    }
  );
  User: UserService;
  locationService: LocationService;
  formbuilder: FormBuilder;
  nav: NavController;
  accountService: AccountService;
  commonService: CommonService;
  modal: ModalController;
  toast: ToastController;
  datePipe: DatePipe;

  constructor(
    private injector: Injector,

  ) {
    this.User = this.injector.get<UserService>(UserService);
    this.locationService = this.injector.get<LocationService>(LocationService);
    this.accountService = this.injector.get<AccountService>(AccountService);
    this.commonService = this.injector.get<CommonService>(CommonService);
    this.formbuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.nav = this.injector.get<NavController>(NavController);
    this.modal = this.injector.get<ModalController>(ModalController);
    this.toast = this.injector.get<ToastController>(ToastController);
    this.datePipe = this.injector.get<DatePipe>(DatePipe);
  }

  ngOnInit(): void {

    this.getCountries();
    const user = this.User.user;
    const userId = user?.userId;
    const role = user?.role;
    if (role === 'DOCTOR') {
      this.status.next(!this.status.getValue());
      this.accountService.getProfilDoctor(userId).subscribe(
        (doctor: any) => {
          this.getForm(doctor);
          this.profilForm.controls.fonction.setValue(doctor.fonction);
          this.profilForm.controls.office.setValue(doctor.office);
          this.profilForm.controls.adresse.setValue(doctor.adresse);
        }
      );
    } else if (role === 'PATIENT') {
      this.status.next(this.status.getValue());
      this.accountService.getProfilUser(userId).subscribe(
        (user: any) => {
          this.getForm(user);
          this.profilForm.controls.birthdate.setValue(user.birthdate);
          this.profilForm.controls.groupeSanguin.setValue(user.groupeSanguin);
          this.profilForm.controls.sexe.setValue(user.genre);
          this.city = user.city;
        }
      );
    }
  }

  /*
  * get countries
  */
  getCountries() {
    this.locationService.getCountries().subscribe(
      (res: any) => {
        this.countries = res.content;
      },
    );
  };

  /*
  * get cities of country
  */
  getCities(countryId: number) {
    this.locationService.getCity(countryId).subscribe(
      (res: any) => {
        this.cities = res.content;
      }
    );
  };

  selectCountryEvent(country: Country) {
    this.getCities(country.id);
  };


  selectCityEvent(city: Country) {
    this.selectedCity = city;
  };

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  };

  onFocused(e) {
    // do something
  };

  /*
  *  get form data
  */
  getForm(data: any) {
    // console.log(data);
    this.profilForm.controls.firstname.setValue(data.firstname);
    this.profilForm.controls.lastname.setValue(data.name);
    this.profilForm.controls.email.setValue(data.email);
    this.profilForm.controls.phone.setValue(data.phone);
  }

  get f(): { [key: string]: AbstractControl } {
    return this.profilForm.controls;
  }

  /*
  * update profile
  */
  update(): void {
    this.switch = !this.switch;
    this.readonly = !this.readonly;
  }

  /*
  * submit
  */
  onSubmit(): void {
    // console.log(this.selectedCity);
    this.switch = !this.switch;
    this.readonly = true;
    this.error.next(null);
    if (!this.submit.getValue()) { this.submit.next(true); }
    const user = this.User.user;
    const userId = user?.userId;
    const role = user?.role;
    if (this.submit.getValue() && !this.loading.getValue()) {
      if (role === 'PATIENT') {
        if (
          this.profilForm.controls.email.valid &&
          this.profilForm.controls.birthdate.valid &&
          this.profilForm.controls.firstname.valid &&
          this.profilForm.controls.lastname.valid &&
          this.profilForm.controls.phone.valid === true) {
          this.loading.next(true);
          // console.log('test');
          this.accountService.updateProfilUser(userId,
            {
              email: this.profilForm.value.email,
              birthdate: this.datePipe.transform(
                this.profilForm.value.birthdate,
                'yyyy-MM-dd'
              ),
              cityId: this.selectedCity?.id?.toString(),
              firstname: this.profilForm.value.firstname,
              name: this.profilForm.value.lastname,
              phone: this.profilForm.value.phone,
              groupeSanguin: this.profilForm.value.groupeSanguin,
              genre: this.profilForm.value.sexe,
            }).subscribe(
              (res: any) => {
                this.loading.next(true);
                this.submit.next(false);
                this.notify();
              },
              error => {
                this.loading.next(false);
                this.submit.next(false);
              }
            );
        }
      } else if (role === 'DOCTOR') {
        if (this.profilForm.controls.email.valid &&
          this.profilForm.controls.firstname.valid &&
          this.profilForm.controls.lastname.valid &&
          this.profilForm.controls.phone.valid === true) {
          this.loading.next(true);
          this.accountService.updateProfilDoctor(userId,
            {
              email: this.profilForm.value.email,
              fonction: this.profilForm.value.fonction,
              office: this.profilForm.value.office,
              cityId: this.selectedCity?.id,
              firstname: this.profilForm.value.firstname,
              name: this.profilForm.value.lastname,
              phone: this.profilForm.value.phone,
              adresse: this.profilForm.value.adresse,
            })
            .subscribe(
              (res: any) => {
                this.loading.next(true);
                this.submit.next(false);
                this.notify();
              },
              error => {
                this.loading.next(false);
                this.submit.next(false);
              }
            );
        }
      }
    } else {
      this.loading.next(false);
    }
  }

  /*
  * send notification
  */
  async notify() {
    const toast = await this.toast.create({
      message: 'Modifié avec succes',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  /*
  * display avatar
  */
  async openOptionSelection() {
    const modal = await this.modal.create({
      component: ProfileOptionPhotoComponent,
      cssClass: 'transparent-modal'
    });
    modal.onDidDismiss()
      .then(res => {
        // console.log(res);
        if (res.role !== 'backdrop') {
          this.takePicture(res.data);
        }
      });
    return await modal.present();
  }
  takePicture(data: any) {
    throw new Error('Method not implemented.');
  }

  selected(item: string): void {
    this.profilForm.controls.fonction.setValue(item);

    this.activities = [];
  }

}

