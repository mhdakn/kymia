import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PwdforgetPage } from './pwdforget.page';

const routes: Routes = [
  {
    path: '',
    component: PwdforgetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PwdforgetPageRoutingModule {}
