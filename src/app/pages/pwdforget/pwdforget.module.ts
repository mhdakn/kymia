import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PwdforgetPageRoutingModule } from './pwdforget-routing.module';

import { PwdforgetPage } from './pwdforget.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PwdforgetPageRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  declarations: [PwdforgetPage]
})
export class PwdforgetPageModule {}
