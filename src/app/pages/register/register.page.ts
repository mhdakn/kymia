/* eslint-disable max-len */
import { Component, Injector, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { UserService as RegisterService } from 'src/app/services/user.service';
import { CommonService } from 'src/app/services/common.service';
import { MustMatch } from './MustMatch.validator';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterComponent implements OnInit {
  status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  registerForm: FormGroup;
  isValidEmail: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  submit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  formBuilder: FormBuilder;
  nav: NavController;
  registerService: RegisterService;
  commonService: CommonService;
  error: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  activities: any[] = [];
  speciality :string = '';

  /*****
    * Constructor
  ****/
  constructor(
    private injector: Injector,
    private storage: StorageService
  ) {
    this.formBuilder = this.injector.get<FormBuilder>(FormBuilder);
    this.nav = this.injector.get<NavController>(NavController);
    this.registerService = this.injector.get<RegisterService>(RegisterService);
    this.commonService = this.injector.get<CommonService>(CommonService);
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]
      ],
      confirmPassword: ['', Validators.required],
      birthdate: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      phone: ['', Validators.required],
      activity: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  async inputChanged(): Promise<void> {
    const value = this.speciality;

    if (value.length <= 0) {
      this.activities = [];
      return;
    }

    this.commonService.getActivities().subscribe(
      (data: any) => {
        const list = data.content;
        const items = list.filter(
          item => item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        );

        this.activities = items;
      },
      (error: any) => {
        // console.log(error);
      }
    );
  }

  selected(item: string): void {
    this.registerForm.controls.activity.setValue(item);

    this.activities = [];
  }

  get f(): { [key: string]: AbstractControl } {
    return this.registerForm.controls;
  }

  /*
  * go to login page
  */
  goToLogin(): void {
    this.nav.navigateForward('/login');
  }

  /*
  *  go to the home page
  */
  goToHome(): void {
    this.nav.navigateRoot('tabs/home');
  }

  goToValidation(): void {
    this.nav.navigateRoot(['/validation']);
  }

  /*
  * @description: submit the register form
  */
  onSubmit(): void {
    this.error.next(null);

    if (!this.submit.getValue()) { this.submit.next(true); }

    if (this.submit.getValue() && !this.loading.getValue()) {
      if (this.registerForm.controls.email.valid && this.registerForm.controls.password.valid &&
        this.registerForm.controls.confirmPassword && this.registerForm.controls.birthdate.valid &&
        this.registerForm.controls.firstname.valid && this.registerForm.controls.lastname.valid &&
        this.registerForm.controls.phone.valid && this.registerForm.controls.acceptTerms.value && this.registerForm.controls.confirmPassword.valid === true) {
        this.loading.next(true);
        this.registerService.registerUser({
          email: this.registerForm.value.email,
          password: this.registerForm.value.password,
          birthdate: this.registerForm.value.birthdate,
          firstname: this.registerForm.value.firstname,
          name: this.registerForm.value.lastname,
          phone: this.registerForm.value.phone,
        })
          .subscribe(
            (res: any) => {
              this.loading.next(false);
              this.submit.next(false);
              localStorage.email = this.registerForm.value.email;
              localStorage.password = this.registerForm.value.password;
              localStorage.type = 'Patient';
              //this.storage.set('token', res.token);

              this.goToValidation();
            },
            error => {
              this.loading.next(false);
              this.submit.next(false);

              this.error.next(error.error.message);
            }
          );
      } else if (this.registerForm.controls.email.valid &&
        this.registerForm.controls.activity.valid && this.registerForm.controls.activity.valid &&
        this.registerForm.controls.firstname.valid &&
        this.registerForm.controls.lastname.valid && this.registerForm.controls.phone.valid &&
        this.registerForm.controls.acceptTerms.value && this.registerForm.controls.password.valid && this.registerForm.controls.confirmPassword.valid === true
      ) {
        this.loading.next(true);
        this.registerService.registerDoctor(
          {
            email: this.registerForm.value.email,
            password: this.registerForm.value.password,
            firstname: this.registerForm.value.firstname,
            name: this.registerForm.value.lastname,
            phone: this.registerForm.value.phone,
            fonction: this.registerForm.value.activity,

          }
        )
          .subscribe(
            (res: any) => {
              this.loading.next(false);
              this.submit.next(false);
              localStorage.email = this.registerForm.value.email;
              localStorage.password = this.registerForm.value.password;
              localStorage.type = 'Doctor';
              this.goToValidation();
            },
            error => {
              this.loading.next(false);
              this.submit.next(false);

              this.error.next(error.error.message);
            }
          );
      }
    } else {
      this.loading.next(false);
    }

  }


  /*
  * switch between patient and doctor
  */
  switch(): void {
    this.status.next(!this.status.getValue());
  }
}
