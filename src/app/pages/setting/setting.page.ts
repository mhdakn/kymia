import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';



@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
  firstname: string;
  lastname: string;

  constructor(
    private User: UserService,
    public navCtrl: NavController,
    private logoutService: UserService,
  ) { }

  ngOnInit() {
    const user = this.User.user;
   
    //get user lastname and firstname
    if(user.role === "PATIENT"){
      this.firstname = user?.firstname;
      this.lastname = user?.lastname;
    }else if(user.role === 'DOCTOR'){
      this.firstname = user?.firstname;
      this.lastname = user?.lastname;
    }
  }
  goToProfil(){
    this.navCtrl.navigateRoot('/profile');
  }
  goToNewpwd(){
    this.navCtrl.navigateRoot('/newpwd');
  }
  goToLogin(){
    this.navCtrl.navigateRoot('/login');
  }
  deconnect(){
    this.logoutService.logout().subscribe(
      (res:any) => {
        this.goToLogin();
      }
    )
  }
 

}

