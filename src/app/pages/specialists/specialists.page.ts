import { Component, OnInit, Injector } from '@angular/core';
import { DoctorService } from 'src/app/services/doctor.service';
import { CommonService } from 'src/app/services/common.service';
import { ReturnStatement } from '@angular/compiler';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';



@Component({
  selector: 'app-specialists',
  templateUrl: './specialists.page.html',
  styleUrls: ['./specialists.page.scss'],
})
export class SpecialistsPage implements OnInit {
  searchTerm: string;
  items: any = [];
  list: any
  images: string;
  title: string;
  description: string;
  doctor: string;
  id: number;
  nav: NavController;
  doctors_list: any =  [];
  alldoctors: any = [];
  search: any = [];


  constructor(
    public doctorService: DoctorService,
    public commonService: CommonService,
    private storage: StorageService,
    private router: Router,
    private injector: Injector
  ) {
    this.nav = this.injector.get<NavController>(NavController);
  }

    async ngOnInit() {
      try {
        this.id = this.router.getCurrentNavigation().extras.state.p3;  
        console.log(this.id);
              
        
        this.doctorService.getSpecialityDoctors(this.id).subscribe(
          (res: any) => {
            
            this.doctors_list = res;
            
            //this.alldoctors = res.content;
  
            return this.alldoctors;
  
          }
        )  

      } catch (error) {

      }

      this.storage.init();
      const user = await this.storage.get('user');
      const userId = user?.userId; 
    }

  gotoApointement() {
    //this.nav.navigateRoot(['/taking'], { state: { id: this.id, p2: false, p3: true, p4:false, p5:false } });
    this.router.navigate(['/taking'], { state: { id: this.id, p2: false, p3: true, p4:false, p5:false } });

    
  }

  goToAlldoctors(){
    this.nav.navigateRoot('/doctors');
  }

  gotoDetails(doctor){
    // console.log(doctor)
    this.nav.navigateRoot(['/details'], {state:{doctorID: doctor.id}});

    //this.nav.navigateRoot('/details'), {state:{p1: item.images,p2: item.title,p3:item.id}});
  }

  goToSpecilities(){
    this.nav.navigateRoot(['/specialities']);
  }
}

