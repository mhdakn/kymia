import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SpecialitiesPageRoutingModule } from './specialities-routing.module';
import { SpecialitiesPage } from './specialities.page';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { SharedModule } from 'src/app/components/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SpecialitiesPageRoutingModule,
    Ng2SearchPipeModule,
    SharedModule
  ],
  declarations: [SpecialitiesPage],
  entryComponents : [],
  providers: []

})
export class SpecialitiesPageModule {}
