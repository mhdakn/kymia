import { Component, OnInit, Injector } from '@angular/core';
import {CommonService} from 'src/app/services/common.service';
import { NavController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-specialities',
  templateUrl: './specialities.page.html',
  styleUrls: ['./specialities.page.scss'],
})
export class SpecialitiesPage implements OnInit {
  searchTerm: string;
  // items = [
  //   {
  //     images: "../../../../assets/img/speciality-image/Vector.svg",
  //     title: "Généraliste"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/ion_paw-outline.svg",
  //     title: "Vétérinaire"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/ic_baseline-child-care.svg",
  //     title: "Pédiatre"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/la_tooth.svg",
  //     title: "Dentiste"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group.svg",
  //     title: "Epidémiologiste"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group_1.svg",
  //     title: "Cardiologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group_2.svg",
  //     title: "Gynécologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/ion_paw-outline.svg",
  //     title: "Dermatologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/ic_baseline-child-care.svg",
  //     title: "Ophtamologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/la_tooth.svg",
  //     title: "Neurologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group.svg",
  //     title: "Radiologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group_1.svg",
  //     title: "Pneumologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/la_tooth.svg",
  //     title: "Rhumatologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group.svg",
  //     title: "Immunologue"
  //   },
  //   {
  //     images: "../../../../assets/img/speciality-image/Group_1.svg",
  //     title: "Anesthésiologie"
  //   }

  // ]


  //
  items:any = [];
  nav: any;

  constructor(public commonService: CommonService, private router: Router, private injector: Injector,) { 
    this.nav = this.injector.get<NavController>(NavController);
    }

  ngOnInit() {
    this.commonService.getActivities().subscribe(
      (data:any) =>{
        const list = data.content;
        

        for (let index = 0; index < list.length; index++) {
          const element = list[index];
          this.items.push(
            {
              id: element.id,
              images: element.icon,
              title: element.name,
              description: element.description,
            }
          );
          
        }
      }
    )
  }

  setSpecialities(item){
    this.nav.navigateRoot(['specialists'], {state:{p3:item.id}});
  }
}
