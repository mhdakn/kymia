import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroGuard } from 'src/app/guards/intro.guard';
import { TabnavPage } from './tabnav.page';

const routes: Routes = [
  {
    path: '',
    component: TabnavPage,
    children: [

      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule),
        canLoad: [IntroGuard]
      },
      {
        path: 'folders',
        loadChildren: () => import('../folders/folders.module').then(m => m.FoldersPageModule),
      },
      {
        path: 'meetings',
        loadChildren: () => import('../meetings/meetings.module').then(m => m.MeetingsPageModule),
      },
      {
        path: 'setting',
        loadChildren: () => import('../setting/setting.module').then(m => m.SettingPageModule),
        canLoad: [IntroGuard]
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabnavPageRoutingModule { }
