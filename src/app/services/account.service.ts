import { StorageService } from 'src/app/services/storage.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DoctorDto, UserDto } from 'src/app/models/request.model';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class AccountService {
  api = environment.base_url;
  token;

  constructor(
    public http: HttpClient,
    private userService: UserService,
  ) {
  }

  // get user
  getProfilUser(id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    const requestOptions = { headers };

    return this.http.get(`${this.api}/patients/` + id, requestOptions);
  }

  /// get doctor
  getProfilDoctor(id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    const requestOptions = { headers };

    return this.http.get(`${this.api}/doctors/` + id, requestOptions);
  }

  // update user
  updateProfilUser(id: number, user: UserDto): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    const requestOptions = { headers };

    return this.http.put(`${this.api}/patients/` + id, user, requestOptions);
  }

  // update doctor
  updateProfilDoctor(id: number, doctor: DoctorDto): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    const requestOptions = { headers };

    return this.http.put(`${this.api}/doctors/` + id, doctor, requestOptions);
  }

}
