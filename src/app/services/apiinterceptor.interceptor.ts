import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

export const API_TOKEN = "";
@Injectable()
export class ApiinterceptorInterceptor implements HttpInterceptor {


  token = "";
  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {


    return next.handle(request.clone({ setHeaders: {
      Authorization: 'Bearer '+this.token} }));
  }
}
