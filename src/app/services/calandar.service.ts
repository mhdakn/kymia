import { StorageService } from 'src/app/services/storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RequestCalandar } from 'src/app/models/request.model';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class CalandarService {
  api = environment.base_url;
  current_doctor_id: number;
  token;

  constructor(
    public http: HttpClient,
    private userService: UserService,
  ) {

  }

  calandarSearch(search: RequestCalandar): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.post(`${this.api}/calendars/search`, search, { headers });
  }
}
