import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class CommonService {
    private id = new BehaviorSubject<number>(1);

    api = environment.base_url;
    constructor(
        public http: HttpClient,
    ) { }

    getActivities(): Observable<unknown> {
        return this.http.get(`${this.api}/domains`);
    }

    searchActivity(term: string): Observable<unknown> {
        return this.http.get(`${this.api}/domains/functions/${term}`);
    }

}




