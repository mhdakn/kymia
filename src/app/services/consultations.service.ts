import { StorageService } from 'src/app/services/storage.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ConsultationData } from 'src/app/models/consultation.model';
import { UserService } from './user.service';



@Injectable({
  providedIn: 'root'
})
export class ConsultationsService {
  api = environment.base_url;

  constructor(
    public http: HttpClient,
    private userService: UserService,
  ) {
  }

  registerConsultation(consultation: ConsultationData): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token()}`);
    return this.http.post(`${this.api}/consultations`, consultation, { headers });
  }

  getAllByDoctorAndPatients(doctor_id: number, patient_id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/consultations/doctor/` + doctor_id + '/patient/' + patient_id, { headers });
  }

}
