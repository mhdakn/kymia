import { StorageService } from 'src/app/services/storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';

@Injectable()
export class DoctorService {
  api = environment.base_url;
  current_doctor_id: number;
  token;

  constructor(
    public http: HttpClient,
    private userService: UserService
  ) {
  }

  getDoctor(id: number): Observable<unknown> {
    this.current_doctor_id = id;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/doctors/` + id, { headers });
  }

  getDoctorCalandar(id: number, available = true): Observable<unknown> {
    this.current_doctor_id = id;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/calendars/doctor/` + id + "?avalability=" + available, { headers });
  }
  getAllDoctors(): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/doctors`, { headers });
  }

  getSpecialityDoctors(id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/domains/` + id , { headers });
  }




  getDoctorID() {
    return this.current_doctor_id;
  }

}
