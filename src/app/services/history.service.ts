import { StorageService } from 'src/app/services/storage.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  api = environment.base_url;
  token: any;

  constructor(
    public http: HttpClient,
    private userService: UserService
  ) {
  }

  getConsultation(id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.userService.token}`);
    return this.http.get(`${this.api}/consultations/patient/` + id, { headers });
  }
}
