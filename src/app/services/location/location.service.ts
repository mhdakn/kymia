import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  api = environment.base_url;

  constructor(
    public http: HttpClient,
  ) { }

  getCountries(): Observable<Injectable> {
    return this.http.get(`${this.api}/countries`);
  }

  getCities(): Observable<Injectable> {
    return this.http.get(`${this.api}/cities`);
  }

  getCity(countryId: number): Observable<Injectable> {
    return this.http.get(`${this.api}/cities/country/${countryId}`);
  }
}
