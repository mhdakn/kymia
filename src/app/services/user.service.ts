import { StorageService } from 'src/app/services/storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestDoctor, RequestUser } from 'src/app/models/request.model';
import { environment } from 'src/environments/environment';
import { RequestCalandar } from 'src/app/models/request.model';
import { ConsultationData } from 'src/app/models/consultation.model';

@Injectable({ providedIn: 'root' })
export class UserService {
  api = environment.base_url;
  isLogged = false;
  user: any;
  token;

  constructor(
    public http: HttpClient,
  ) {
  }

  login({ email, password }: { email: string; password: string }): Observable<unknown> {
    return this.http.post(`${this.api}/users/login`, { email, password });
  }

  logout(): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
    return this.http.get(`${this.api}/users/logout`, { headers });
  }

  resetPassord(email: string): Observable<unknown> {
    return this.http.put(`${this.api}/users/password/reset?mail=${encodeURI(email)}`, {});
  }

  newpwd(id, { oldPassword, newPassword }): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
    return this.http.put(`${this.api}/users/password/update/` + id + '?newPassword=' + newPassword + '&oldPassword=' + oldPassword, {},
      { headers });
  }

  registerUser(user: RequestUser): Observable<unknown> {
    return this.http.post(`${this.api}/patients`, user);
  }

  registerDoctor(doctor: RequestDoctor): Observable<unknown> {
    return this.http.post(`${this.api}/doctors`, doctor);
  }

  validation({ email, code }: { email: string; code: string }): Observable<unknown> {
    return this.http.put(`${this.api}/users/signup/validation`, { email, code });
  }
  resend(email: string): Observable<unknown> {
    return this.http.put(`${this.api}/users/validation/resend`, { email });
  }

  //calandar
  calandarSearch(search: RequestCalandar): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
    return this.http.post(`${this.api}/calendars/search`, search, { headers });
  }


  //consulatation
  registerConsultation(consultation: ConsultationData): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
    return this.http.post(`${this.api}/consultations`, consultation, { headers });
  }

  getAllByDoctorAndPatients(doctor_id: number, patient_id: number): Observable<unknown> {
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.token}`);
    return this.http.get(`${this.api}/consultations/doctor/` + doctor_id + '/patient/' + patient_id, { headers });
  }
}
